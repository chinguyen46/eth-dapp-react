import React from 'react';
import './App.css';
import Web3 from 'web3';
import { TODO_LIST_ABI, TODO_LIST_ADDRESS } from './config'


class App extends React.Component {

  state = { account: '', taskCount: 0 };

  componentDidMount(){
    this.loadBlockChainData();
  }

  async loadBlockChainData(){
    const web3 = new Web3(Web3.givenProvider || 'http://localhost:3000');
    const network = await web3.eth.net.getNetworkType();
    const ethereum = window.ethereum;
    const enabledWeb3 = await ethereum.enable();
    const accounts = await web3.eth.getAccounts();
    
    this.setState({ account: accounts[0] });
    const todoList = new web3.eth.Contract(TODO_LIST_ABI, TODO_LIST_ADDRESS);
    this.setState({ todoList: todoList})
    const taskCount = await todoList.methods.taskCount().call();
    this.setState({ taskCount: taskCount });
  }


  render(){
    return (
      <div className="container">
        <h1>Hello world</h1>
        <p>Account number: {this.state.account}</p>
        <p>Task count: {this.state.taskCount}</p>
      </div>
    );
  }
}


export default App;
